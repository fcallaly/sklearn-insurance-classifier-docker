## Simple Sklearn Classifier

This project contains an ML model built with sklearn that predicts insurance costs based on historic data.

Insurance costs are are split into 4 classes:

- class 0: less than $3000 per year
- class 1: between $3000 and $5000 per year
- class 2: between $5000 and $10000 per year
- class 3: greater than $10000 per year

By default the app will listen on port 8081.

The app is exposed as a REST API, with a swagger ui to allow you to test the UI.


E.g. use the swagger ui to post:

```
{
  "age": 21,
  "gender_code": 0,
  "bmi": 21,
  "children": 0,
  "smoker_code": 0
}
```
i.e. a 21 year old non-smoker with 0 children -> expect class 0


-------

# Building as a Docker image

To build and run as a docker image:

1. Examine the Dockerfile to understand it
2. Build an image with Docker:
    - ```docker build -t py-insurance:0.0.1 .``` (Don't forget the trailing '.')
3. Run the image with Docker:
    - ```docker run --name py-insurance -d -p 8081:8081 py-insurance:0.0.1```
4. Verify you can access the app on port 8081
5. Use the swagger ui to test that the app is predicting insurance classes
6. Check the app logs with ```docker logs py-insurance```
7. Get a bash command line INSIDE the container with ```docker exec -it py-insurance /bin/bash```
    - ```cd app``` to verify that the python code is in the container
8. Use ```docker history py-insurance:0.0.1``` to see the history of the layers added to the docker image